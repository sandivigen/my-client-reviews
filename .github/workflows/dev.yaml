name: MCR deploy (dev)

on:
  push:
    branches: [ dev ]

  # Allows you to run this workflow manually from the Actions tab
  workflow_dispatch:

env:
  ENV_DEV_FILE: .env.dev
  ENV_PROD_FILE: .env.prod
  AWS_REGION: ap-southeast-2

defaults:
  run:
    shell: bash

jobs:
  build:
    name: Build docker containers
    runs-on: ubuntu-latest
    permissions:
      packages: write
      contents: read

    steps:
      - name: Checkout
        uses: actions/checkout@v2
        
      - name: Coping .env.template 
        id: copy_env
        run: |
          cp .env.template .env    
          
      - name: Setting up .env file (prod)
        id: setup_env
        run: |
          echo "DB_CONNECTION_STRING=${{ secrets.DEV_DB_CONNECTION_STRING }}" >> .env
          echo "MONGO_INITDB_ROOT_USERNAME=${{ secrets.DEV_MONGO_INITDB_ROOT_USERNAME }}" >> .env
          echo "MONGO_INITDB_ROOT_PASSWORD=${{ secrets.DEV_MONGO_INITDB_ROOT_PASSWORD }}" >> .env
          echo "JWT_AUTH_SECRET=${{ secrets.DEV_JWT_AUTH_SECRET }}" >> .env
          echo "JWT_COMMON_SECRET=${{ secrets.DEV_JWT_COMMON_SECRET }}" >> .env
          echo "BCC_EMAIL=${{ secrets.DEV_BCC_EMAIL }}" >> .env
          echo "DEBUG=${{ secrets.DEV_DEBUG }}" >> .env
          echo "MAIL_HOST=${{ secrets.MAIL_HOST }}" >> .env
          echo "MAIL_USER=${{ secrets.DEV_MAIL_USER }}" >> .env
          echo "MAIL_PASS=${{ secrets.DEV_MAIL_PASS }}" >> .env
          echo "MAIL_FROM=${{ secrets.DEV_MAIL_FROM }}" >> .env
          echo "S3_ACCESS_KEY_ID=${{ secrets.DEV_S3_ACCESS_KEY_ID }}" >> .env
          echo "S3_SECRET_ACCESS_KEY=${{ secrets.DEV_S3_SECRET_ACCESS_KEY }}" >> .env
          echo "S3_BUCKET=${{ secrets.DEV_S3_BUCKET }}" >> .env
          echo "STRIPE_PUBLIC_KEY=${{ secrets.DEV_STRIPE_PUBLIC_KEY }}" >> .env
          echo "STRIPE_SECRET_KEY=${{ secrets.DEV_STRIPE_SECRET_KEY }}" >> .env
          echo "STRIPE_WEBHOOK_SECRET_KEY=${{ secrets.DEV_STRIPE_WEBHOOK_SECRET_KEY }}" >> .env
          echo "BILLING_DEFAULT_TAX_RATE=${{ secrets.DEV_BILLING_DEFAULT_TAX_RATE }}" >> .env
          echo "LIVECHAT_LICENSE=${{ secrets.DEV_LIVECHAT_LICENSE }}" >> .env
          echo "VITE_GOOGLE_API_KEY=${{ secrets.DEV_VITE_GOOGLE_API_KEY }}" >> .env
          echo "VITE_GOOGLE_API_KEY=${{ secrets.DEV_VITE_GOOGLE_API_KEY }}" >> .env
          echo "FRONTEND_EXTERNAL_BASE_URL=${{ secrets.DEV_FRONTEND_EXTERNAL_BASE_URL }}" >> .env
          echo "S3_BUCKET_IMAGES=${{ secrets.DEV_BACKEND_EXTERNAL_BASE_URL }}" >> .env
          echo "VITE_API_FRONTEND_BASE_URL=${{ secrets.DEV_VITE_API_FRONTEND_BASE_URL }}" >> .env
          echo "STRIPE_PRODUCT_ID=${{ secrets.DEV_STRIPE_PRODUCT_ID }}" >> .env
          echo "VERIFY_EMAIL_BASE_URL=${{ secrets.DEV_VERIFY_EMAIL_BASE_URL }}" >> .env
          echo "RESET_PASSWORD_BASE_URL=${{ secrets.DEV_RESET_PASSWORD_BASE_URL }}" >> .env
          echo "FRONTEND_INTERNAL_PORT=80" >> .env
          echo "BACKEND_INTERNAL_PORT=80" >> .env
          echo "VITE_API_BACKEND_BASE_URL=${{ secrets.DEV_VITE_API_BACKEND_BASE_URL }}" >> .env
          echo "VITE_FINGERPRINT_PUBLIC_TOKEN=${{ secrets.DEV_VITE_FINGERPRINT_PUBLIC_TOKEN }}" >> .env
          echo "BACKEND_ROUTE_PREFIX=" >> .env
          echo "VITE_CSV_EMAILS_LIMIT=1000" >> .env
          echo "LIMIT_FROM_LAST_INVITATION_IN_MINUTES=129600" >> .env
          echo "MAIL_SERVICE_TEST_MODE=false" >> .env
          echo "CLOUD_WATCH_LOGS_REGION=${{ secrets.CLOUD_WATCH_LOGS_REGION }}" >> .env
          echo "CLOUD_WATCH_LOGS_GROUP_NAME=${{ secrets.CLOUD_WATCH_LOGS_GROUP_NAME }}" >> .env
          echo "CLOUD_WATCH_LOGS_RECORDS_LIMIT=${{ secrets.CLOUD_WATCH_LOGS_RECORDS_LIMIT }}" >> .env
          echo "CLOUD_WATCH_LOGS_START_SECONDS_OFFSET=${{ secrets.CLOUD_WATCH_LOGS_START_SECONDS_OFFSET }}" >> .env
          echo "CLOUD_WATCH_LOGS_ACCESS_KEY_ID=${{ secrets.DEV_CLOUD_WATCH_LOGS_ACCESS_KEY_ID }}" >> .env
          echo "CLOUD_WATCH_LOGS_SECRET_ACCESS_KEY=${{ secrets.DEV_CLOUD_WATCH_LOGS_SECRET_ACCESS_KEY }}" >> .env
          echo "CLOUD_WATCH_LOGS_TO_EMAIL=${{ secrets.CLOUD_WATCH_LOGS_TO_EMAIL }}" >> .env
          echo "MAIL_SUPPORT_EMAIL=1612397438@tickets.helpdesk.com" >> .env

      - name: Configure AWS credentials
        uses: aws-actions/configure-aws-credentials@v1
        with:
          aws-access-key-id: ${{ secrets.AWS_ACCESS_KEY_ID }}
          aws-secret-access-key: ${{ secrets.AWS_SECRET_ACCESS_KEY }}
          aws-region: ${{ env.AWS_REGION }}

      - name: Login to Amazon ECR
        id: login-ecr
        uses: aws-actions/amazon-ecr-login@v1

      - name: Build, tag, and push image to Amazon ECR
        id: build-image
        env:
          ECR_REGISTRY_FRONTEND: 229425579640.dkr.ecr.ap-southeast-2.amazonaws.com/mcr_frontend
          ECR_REGISTRY_BACKEND: 229425579640.dkr.ecr.ap-southeast-2.amazonaws.com/mcr_backend
          IMAGE_TAG: dev
        run: |
          # Build a docker container and
          # push it to ECR so that it can
          # be deployed to ECS.
          docker-compose --env-file .env -p 'myclientreviews' -f docker-compose.yml -f docker-compose.dev.yml build
          docker push $ECR_REGISTRY_FRONTEND:$IMAGE_TAG
          docker push $ECR_REGISTRY_BACKEND:$IMAGE_TAG
               
  deploy:
    name: Deploy docker containers
    runs-on: mcr-dev
    needs: build
    
    steps:
      - name: Removal of files
        id: removal_of_files
        run: |
          rm -rf ./* 2> /dev/null || true

      - name: Checkout
        uses: actions/checkout@v2

      - name: Coping .env.template
        id: copy_env
        run: |
          rm .env 2> /dev/null || true
          cp .env.template .env

      - name: Setting up .env file (prod)
        id: setup_env
        run: |
          echo "DB_CONNECTION_STRING=${{ secrets.DEV_DB_CONNECTION_STRING }}" >> .env
          echo "MONGO_INITDB_ROOT_USERNAME=${{ secrets.DEV_MONGO_INITDB_ROOT_USERNAME }}" >> .env
          echo "MONGO_INITDB_ROOT_PASSWORD=${{ secrets.DEV_MONGO_INITDB_ROOT_PASSWORD }}" >> .env
          echo "JWT_AUTH_SECRET=${{ secrets.DEV_JWT_AUTH_SECRET }}" >> .env
          echo "JWT_COMMON_SECRET=${{ secrets.DEV_JWT_COMMON_SECRET }}" >> .env
          echo "BCC_EMAIL=${{ secrets.DEV_BCC_EMAIL }}" >> .env
          echo "DEBUG=${{ secrets.DEV_DEBUG }}" >> .env
          echo "MAIL_HOST=${{ secrets.MAIL_HOST }}" >> .env
          echo "MAIL_USER=${{ secrets.DEV_MAIL_USER }}" >> .env
          echo "MAIL_PASS=${{ secrets.DEV_MAIL_PASS }}" >> .env
          echo "MAIL_FROM=${{ secrets.DEV_MAIL_FROM }}" >> .env
          echo "S3_ACCESS_KEY_ID=${{ secrets.DEV_S3_ACCESS_KEY_ID }}" >> .env
          echo "S3_SECRET_ACCESS_KEY=${{ secrets.DEV_S3_SECRET_ACCESS_KEY }}" >> .env
          echo "S3_BUCKET=${{ secrets.DEV_S3_BUCKET }}" >> .env
          echo "STRIPE_PUBLIC_KEY=${{ secrets.DEV_STRIPE_PUBLIC_KEY }}" >> .env
          echo "STRIPE_SECRET_KEY=${{ secrets.DEV_STRIPE_SECRET_KEY }}" >> .env
          echo "STRIPE_WEBHOOK_SECRET_KEY=${{ secrets.DEV_STRIPE_WEBHOOK_SECRET_KEY }}" >> .env
          echo "BILLING_DEFAULT_TAX_RATE=${{ secrets.DEV_BILLING_DEFAULT_TAX_RATE }}" >> .env
          echo "LIVECHAT_LICENSE=${{ secrets.DEV_LIVECHAT_LICENSE }}" >> .env
          echo "VITE_GOOGLE_API_KEY=${{ secrets.DEV_VITE_GOOGLE_API_KEY }}" >> .env
          echo "FRONTEND_EXTERNAL_BASE_URL=${{ secrets.DEV_FRONTEND_EXTERNAL_BASE_URL }}" >> .env
          echo "S3_BUCKET_IMAGES=${{ secrets.DEV_BACKEND_EXTERNAL_BASE_URL }}" >> .env
          echo "VITE_API_FRONTEND_BASE_URL=${{ secrets.DEV_VITE_API_FRONTEND_BASE_URL }}" >> .env
          echo "STRIPE_PRODUCT_ID=${{ secrets.DEV_STRIPE_PRODUCT_ID }}" >> .env
          echo "VERIFY_EMAIL_BASE_URL=${{ secrets.DEV_VERIFY_EMAIL_BASE_URL }}" >> .env
          echo "RESET_PASSWORD_BASE_URL=${{ secrets.DEV_RESET_PASSWORD_BASE_URL }}" >> .env
          echo "FRONTEND_INTERNAL_PORT=80" >> .env
          echo "BACKEND_INTERNAL_PORT=80" >> .env
          echo "VITE_API_BACKEND_BASE_URL=${{ secrets.DEV_VITE_API_BACKEND_BASE_URL }}" >> .env
          echo "VITE_FINGERPRINT_PUBLIC_TOKEN=${{ secrets.DEV_VITE_FINGERPRINT_PUBLIC_TOKEN }}" >> .env
          echo "BACKEND_ROUTE_PREFIX=" >> .env
          echo "VITE_CSV_EMAILS_LIMIT=1000" >> .env
          echo "LIMIT_FROM_LAST_INVITATION_IN_MINUTES=129600" >> .env
          echo "CLOUD_WATCH_LOGS_REGION=${{ secrets.CLOUD_WATCH_LOGS_REGION }}" >> .env
          echo "CLOUD_WATCH_LOGS_GROUP_NAME=${{ secrets.CLOUD_WATCH_LOGS_GROUP_NAME }}" >> .env
          echo "CLOUD_WATCH_LOGS_RECORDS_LIMIT=${{ secrets.CLOUD_WATCH_LOGS_RECORDS_LIMIT }}" >> .env
          echo "CLOUD_WATCH_LOGS_START_SECONDS_OFFSET=${{ secrets.CLOUD_WATCH_LOGS_START_SECONDS_OFFSET }}" >> .env
          echo "CLOUD_WATCH_LOGS_ACCESS_KEY_ID=${{ secrets.DEV_CLOUD_WATCH_LOGS_ACCESS_KEY_ID }}" >> .env
          echo "CLOUD_WATCH_LOGS_SECRET_ACCESS_KEY=${{ secrets.DEV_CLOUD_WATCH_LOGS_SECRET_ACCESS_KEY }}" >> .env
          echo "CLOUD_WATCH_LOGS_TO_EMAIL=${{ secrets.CLOUD_WATCH_LOGS_TO_EMAIL }}" >> .env
          echo "MAIL_SERVICE_TEST_MODE=false" >> .env
          echo "MAIL_SUPPORT_EMAIL=1612397438@tickets.helpdesk.com" >> .env

      - name: Run build
        id: run_image
        run: |
          docker-compose --env-file .env -p 'myclientreviews' -f docker-compose.yml -f docker-compose.dev.yml down
          docker system prune -a --force
          docker-compose --env-file .env -p 'myclientreviews' -f docker-compose.yml -f docker-compose.dev.yml pull
          docker-compose --env-file .env -p 'myclientreviews' -f docker-compose.yml -f docker-compose.dev.yml up -d

#       - name: Restoring Mongo backup
#         id: restore_backup
#         run: |
#           docker cp /home/github-runner/dbprod.dump mcr-dev-mongo:/dbprod.dump
#           docker exec -it mcr-dev-mongo mongorestore --username=${{ secrets.DEV_MONGO_INITDB_ROOT_USERNAME }} --password=${{ secrets.DEV_MONGO_INITDB_ROOT_PASSWORD }} --archive=/dbprod.dump
#           docker exec -it mcr-dev-mongo mongorestore rm /dbprod.dump
